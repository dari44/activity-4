public class Student{
	private String name;
	private double grade;
	private int amountLearnt;
	private int section;
	
	public Student(String name, double grade){
		this.amountLearnt = 0;
		this.grade = grade;
		this.name = name;
	}
	
	public void sayHi(){
		System.out.println("Hi!");
	}
	
	public void study(){
		this.grade+= 1;
	}
	
	public void learn(int amountStudied){
		if (amountStudied>0){
			this.amountLearnt +=amountStudied;
		}
	}
	
	public String getName(){
		return this.name;
	}
	
	public double getGrade(){
		return this.grade;
	}
	
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	public int getSection(){
		return this.section;
	}
	
	public void setSection(int section){
		this.section = section;
	}
	

	/*
	public void setName(String newName){
		this.name = newName;
	}
	
	public void setGrade(double grade){
		this.grade = grade;
	}
	*/
	
	
}